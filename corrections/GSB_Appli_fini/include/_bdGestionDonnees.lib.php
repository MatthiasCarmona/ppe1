<?php

/**
 * Regroupe les fonctions d'accès aux données.
 * @package default
 * @author Arthur Martin
 * @todo Fonctions retournant plusieurs lignes sont à réécrire.
 */

/**
 * Se connecte au serveur de données MySql.                      
 * Se connecte au serveur de données MySql à partir de valeurs
 * prédéfinies de connexion (hôte, compte utilisateur et mot de passe). 
 * Retourne l'identifiant de connexion si succès obtenu, le booléen false 
 * si problème de connexion.
 * @return resource identifiant de connexion
 */
function connecterServeurBD() {
    $hote = "localhost";
    $login = "root";
    $mdp = "";
    return mysqli_connect($hote, $login, $mdp);
}

/**
 * Sélectionne (rend active) la base de données.
 * Sélectionne (rend active) la BD prédéfinie gsb_frais sur la connexion
 * identifiée par $idCnx. Retourne true si succès, false sinon.
 * @param resource $idCnx identifiant de connexion
 * @return boolean succès ou échec de sélection BD 
 */
function activerBD($idCnx) {
    $bd = "gsb_frais";
    $query = "SET CHARACTER SET utf8";
    // Modification du jeu de caractères de la connexion
    $res = mysqli_query($idCnx,$query );
    $ok = mysqli_select_db($idCnx,$bd );
    return $ok;
}

/**
 * Ferme la connexion au serveur de données.
 * Ferme la connexion au serveur de données identifiée par l'identifiant de 
 * connexion $idCnx.
 * @param resource $idCnx identifiant de connexion
 * @return void  
 */
function deconnecterServeurBD($idCnx) {
    mysqli_close($idCnx);
}

/**
 * Echappe les caractères spéciaux d'une chaîne.
 * Envoie la chaîne $str échappée, càd avec les caractères considérés spéciaux
 * par MySql (tq la quote simple) précédés d'un \, ce qui annule leur effet spécial
 * @param string $str chaîne à échapper
 * @return string chaîne échappée 
 */

/* Fonction considérer comme obsolète depuis php 5.4

function filtrerChainePourBD($str) {
    if (!get_magic_quotes_gpc()) {
        // si la directive de configuration magic_quotes_gpc est activée dans php.ini,
        // toute chaîne reçue par get, post ou cookie est déjà échappée 
        // par conséquent, il ne faut pas échapper la chaîne une seconde fois                              
        $str = mysqli_real_escape_string($str);
    }
    return $str;
}
*/

function filtrerChainePourBD($str){
		$str = addslashes($str);
		return $str;
}
/**
 * Fournit les informations sur un utilisateur demandé. 
 * Retourne les informations du utilisateur d'id $unId sous la forme d'un tableau
 * associatif dont les clés sont les noms des colonnes(id, nom, prenom).
 * @param resource $idCnx identifiant de connexion
 * @param string $unId id de l'utilisateur
 * @return array  tableau associatif du utilisateur
 */
function obtenirDetailUtilisateur($idCnx, $unId) {
    $id = filtrerChainePourBD($unId);
    $requete = "select utilisateur.id, nom, prenom, TypeUser from utilisateur inner join type_user on utilisateur.idType=type_user.id_type where utilisateur.id='" . $unId . "'";
    $idJeuRes = mysqli_query($idCnx,$requete);
    $ligne = false;
    if ($idJeuRes) {
        $ligne = mysqli_fetch_assoc($idJeuRes);
        mysqli_free_result($idJeuRes);
    }
    return $ligne;
}

/**
 * Fournit les informations d'une fiche de frais. 
 * Retourne les informations de la fiche de frais du mois de $unMois (MMAAAA)
 * sous la forme d'un tableau associatif dont les clés sont les noms des colonnes
 * (nbJustitificatifs, idEtat, libelleEtat, dateModif, montantValide).
 * @param resource $idCnx identifiant de connexion
 * @param string $unMois mois demandé (MMAAAA)
 * @param string $unIdUtilisateur id visiteur  
 * @return array tableau associatif de la fiche de frais
 */
function obtenirDetailFicheFrais($idCnx, $unMois, $unUtilisateur) {
    $unMois = filtrerChainePourBD($unMois);
    $ligne = false;
    $requete = "select IFNULL(nbJustificatifs,0) as nbJustificatifs, Etat.id as idEtat, libelle as libelleEtat, dateModif, montantValide
    from FicheFrais inner join Etat on idEtat = Etat.id 
    where idUtilisateur='" . $unUtilisateur . "' and mois='" . $unMois . "'";
    $idJeuRes = mysqli_query($idCnx,$requete);
    if ($idJeuRes) {
        $ligne = mysqli_fetch_assoc($idJeuRes);
    }
    mysqli_free_result($idJeuRes);

    return $ligne;
}

/**
 * Vérifie si une fiche de frais existe ou non. 
 * Retourne true si la fiche de frais du mois de $unMois (MMAAAA) du visiteur 
 * $idUtilisateur existe, false sinon. 
 * @param resource $idCnx identifiant de connexion
 * @param string $unMois mois demandé (MMAAAA)
 * @param string $unIdUtilisateur id visiteur  
 * @return booléen existence ou non de la fiche de frais
 */
function existeFicheFrais($idCnx, $unMois, $unIdUtilisateur) {
    $unMois = filtrerChainePourBD($unMois);
    $requete = "select idUtilisateur from FicheFrais where idUtilisateur='" . $unIdUtilisateur .
            "' and mois='" . $unMois . "'";
    $idJeuRes = mysqli_query($idCnx,$requete);
    $ligne = false;
    if ($idJeuRes) {
        $ligne = mysqli_fetch_assoc($idJeuRes);
        mysqli_free_result($idJeuRes);
    }

    // si $ligne est un tableau, la fiche de frais existe, sinon elle n'exsite pas
    return is_array($ligne);
}

/**
 * Fournit le mois de la dernière fiche de frais d'un visiteur.
 * Retourne le mois de la dernière fiche de frais du visiteur d'id $unIdUtilisateur.
 * @param resource $idCnx identifiant de connexion
 * @param string $unIdUtilisateur id visiteur  
 * @return string dernier mois sous la forme AAAAMM
 */
function obtenirDernierMoisSaisi($idCnx, $unIdUtilisateur) {
    $requete = "select max(mois) as dernierMois from FicheFrais where idUtilisateur='" .
            $unIdUtilisateur . "'";
    $idJeuRes = mysqli_query($idCnx,$requete );
    $dernierMois = false;
    if ($idJeuRes) {
        $ligne = mysqli_fetch_assoc($idJeuRes);
        $dernierMois = $ligne["dernierMois"];
        mysqli_free_result($idJeuRes);
    }
    return $dernierMois;
}

/**
 * Ajoute une nouvelle fiche de frais et les éléments forfaitisés associés, 
 * Ajoute la fiche de frais du mois de $unMois (MMAAAA) du visiteur 
 * $idUtilisateur, avec les éléments forfaitisés associés dont la quantité initiale
 * est affectée à 0. Clôt éventuellement la fiche de frais précédente du visiteur. 
 * @param resource $idCnx identifiant de connexion
 * @param string $unMois mois demandé (MMAAAA)
 * @param string $unIdUtilisateur id visiteur  
 * @return void
 */
function ajouterFicheFrais($idCnx, $unMois, $unIdUtilisateur) {
    $unMois = filtrerChainePourBD($unMois);
    // modification de la dernière fiche de frais du visiteur
    $dernierMois = obtenirDernierMoisSaisi($idCnx, $unIdUtilisateur);
    $laDerniereFiche = obtenirDetailFicheFrais($idCnx, $dernierMois, $unIdUtilisateur);
    if (is_array($laDerniereFiche) && $laDerniereFiche['idEtat'] == 'CR') {
        modifierEtatFicheFrais($idCnx, $dernierMois, $unIdUtilisateur, 'CL');
    }

    // ajout de la fiche de frais à l'état Créé
    $requete = "insert into FicheFrais (idUtilisateur, mois, nbJustificatifs, montantValide, idEtat, dateModif) values ('"
            . $unIdUtilisateur
            . "','" . $unMois . "',0,NULL, 'CR', '" . date("Y-m-d") . "')";
    mysqli_query($idCnx,$requete);

    // ajout des éléments forfaitisés
    $requete = "select id from FraisForfait";
    $idJeuRes = mysqli_query($idCnx,$requete);
    if ($idJeuRes) {
        $ligne = mysqli_fetch_assoc($idJeuRes);
        while (is_array($ligne)) {
            $idFraisForfait = $ligne["id"];
            // insertion d'une ligne frais forfait dans la base
            $requete = "insert into LigneFraisForfait (idUtilisateur, mois, idFraisForfait, quantite)
                        values ('" . $unIdUtilisateur . "','" . $unMois . "','" . $idFraisForfait . "',0)";
            mysqli_query($idCnx,$requete);
            // passage au frais forfait suivant
            $ligne = mysqli_fetch_assoc($idJeuRes);
        }
        mysqli_free_result($idJeuRes);
    }
}

/**
 * Retourne le texte de la requête select concernant les mois pour lesquels un 
 * visiteur a une fiche de frais. 
 * 
 * La requête de sélection fournie permettra d'obtenir les mois (AAAAMM) pour 
 * lesquels le visiteur $unIdUtilisateur a une fiche de frais. 
 * @param string $unIdUtilisateur id visiteur  
 * @param string $unEtat (facultatif)
 * @return string texte de la requête select
 */
function obtenirReqMoisFicheFrais($unIdUtilisateur, $unEtat = "") {
    if ($unEtat == "") {
        // Utilisation originelle de la fonction
        $req = "select fichefrais.mois as mois from  fichefrais where fichefrais.IdUtilisateur ='"
                . $unIdUtilisateur . "' order by fichefrais.mois desc ";
    } else {
        // On applique une restriction sur l'état
        $req = "select fichefrais.mois as mois from  fichefrais where fichefrais.idUtilisateur ='"
                . $unIdUtilisateur . "' and fichefrais.idetat = '"
                . $unEtat . "' order by fichefrais.mois desc ";
    }
    return $req;
}

/**
 * Retourne le texte de la requête select concernant les éléments forfaitisés 
 * d'un visiteur pour un mois donnés. 
 * 
 * La requête de sélection fournie permettra d'obtenir l'id, le libellé et la
 * quantité des éléments forfaitisés de la fiche de frais du visiteur
 * d'id $IdUtilisateur pour le mois $mois    
 * @param string $unMois mois demandé (MMAAAA)
 * @param string $unIdUtilisateur id visiteur  
 * @return string texte de la requête select
 */
function obtenirReqEltsForfaitFicheFrais($unMois, $unIdUtilisateur) {
    $unMois = filtrerChainePourBD($unMois);
    $requete = "select idFraisForfait, libelle, quantite from LigneFraisForfait
              inner join FraisForfait on FraisForfait.id = LigneFraisForfait.idFraisForfait
              where IdUtilisateur='" . $unIdUtilisateur . "' and mois='" . $unMois . "'";
    return $requete;
}

/**
 * Retourne le texte de la requête select concernant les éléments hors forfait 
 * d'un visiteur pour un mois donnés. 
 * 
 * La requête de sélection fournie permettra d'obtenir l'id, la date, le libellé 
 * et le montant des éléments hors forfait de la fiche de frais du visiteur
 * d'id $idUtilisateur pour le mois $mois    
 * @param string $unMois mois demandé (MMAAAA)
 * @param string $unIdUtilisateur id visiteur  
 * @return string texte de la requête select
 */
function obtenirReqEltsHorsForfaitFicheFrais($unMois, $unIdUtilisateur) {
    $unMois = filtrerChainePourBD($unMois);
    $requete = "select id, date, libelle, montant from LigneFraisHorsForfait
              where idUtilisateur='" . $unIdUtilisateur
            . "' and mois='" . $unMois . "'";
    return $requete;
}

/**
 * Supprime une ligne hors forfait.
 * Supprime dans la BD la ligne hors forfait d'id $unIdLigneHF
 * @param resource $idCnx identifiant de connexion
 * @param string $idLigneHF id de la ligne hors forfait
 * @return void
 */
function supprimerLigneHF($idCnx, $unIdLigneHF) {
    $requete = "delete from LigneFraisHorsForfait where id = " . $unIdLigneHF;
    mysqli_query($idCnx,$requete);
}

/**
 * Ajoute une nouvelle ligne hors forfait.
 * Insère dans la BD la ligne hors forfait de libellé $unLibelleHF du montant 
 * $unMontantHF ayant eu lieu à la date $uneDateHF pour la fiche de frais du mois
 * $unMois du visiteur d'id $unIdUtilisateur
 * @param resource $idCnx identifiant de connexion
 * @param string $unMois mois demandé (AAMMMM)
 * @param string $unIdUtilisateur id du visiteur
 * @param string $uneDateHF date du frais hors forfait
 * @param string $unLibelleHF libellé du frais hors forfait 
 * @param double $unMontantHF montant du frais hors forfait
 * @return void
 */
function ajouterLigneHF($idCnx, $unMois, $unIdUtilisateur, $uneDateHF, $unLibelleHF, $unMontantHF) {
    $unLibelleHF = filtrerChainePourBD($unLibelleHF);
    $uneDateHF = filtrerChainePourBD(convertirDateFrancaisVersAnglais($uneDateHF));
    $unMois = filtrerChainePourBD($unMois);
    $requete = "insert into LigneFraisHorsForfait(idUtilisateur, mois, date, libelle, montant) 
                values ('" . $unIdUtilisateur . "','" . $unMois . "','" . $uneDateHF . "','" . $unLibelleHF . "'," . $unMontantHF . ")";
    mysqli_query($idCnx,$requete );
}

/**
 * Modifie les quantités des éléments forfaitisés d'une fiche de frais. 
 * Met à jour les éléments forfaitisés contenus  
 * dans $desEltsForfaits pour le visiteur $unIdUtilisateur et
 * le mois $unMois dans la table LigneFraisForfait, après avoir filtré 
 * (annulé l'effet de certains caractères considérés comme spéciaux par 
 *  MySql) chaque donnée   
 * @param resource $idCnx identifiant de connexion
 * @param string $unMois mois demandé (MMAAAA) 
 * @param string $unIdUtilisateur  id visiteur
 * @param array $desEltsForfait tableau des quantités des éléments hors forfait
 * avec pour clés les identifiants des frais forfaitisés 
 * @return void  
 */
function modifierEltsForfait($idCnx, $unMois, $unIdUtilisateur, $desEltsForfait) {
    $unMois = filtrerChainePourBD($unMois);
    $unIdUtilisateur = filtrerChainePourBD($unIdUtilisateur);
    foreach ($desEltsForfait as $idFraisForfait => $quantite) {
        $requete = "update LigneFraisForfait set quantite = " . $quantite
                . " where idUtilisateur = '" . $unIdUtilisateur . "' and mois = '"
                . $unMois . "' and idFraisForfait='" . $idFraisForfait . "'";
        mysqli_query($idCnx,$requete);
    }
}

/**
 * Contrôle les informations de connexionn d'un utilisateur.
 * Vérifie si les informations de connexion $unLogin, $unMdp sont ou non valides.
 * Retourne les informations de l'utilisateur sous forme de tableau associatif 
 * dont les clés sont les noms des colonnes (id, nom, prenom, login, mdp)
 * si login et mot de passe existent, le booléen false sinon. 
 * @param resource $idCnx identifiant de connexion
 * @param string $unLogin login 
 * @param string $unMdp mot de passe 
 * @return array tableau associatif ou booléen false 
 */
function verifierInfosConnexion($idCnx, $unLogin, $unMdp) {
    $unLogin = filtrerChainePourBD($unLogin);
    $unMdp = filtrerChainePourBD($unMdp);
	echo $unLogin;
	echo $unMdp;
    // le mot de passe est crypté dans la base avec la fonction de hachage md5
    $req = "select id, nom, prenom, login, mdp from utilisateur where login= ? and mdp= ?";
	$prep = $idCnx->prepare($req);
	$prep->bind_param("ss",$unLogin,$unMdp);
	$idJeuRes = $prep->execute();
	$prep = $prep->get_result();
    //$idJeuRes = mysqli_query($idCnx,$req);
    $ligne = false;
    if ($idJeuRes) {
		$ligne = $prep->fetch_assoc();
        //mysqli_stmt_free_result($idJeuRes);
    }
    return $ligne;
}



/**
 * Modifie l'état et la date de modification d'une fiche de frais
 *
 * Met à jour l'état de la fiche de frais du visiteur $unIdUtilisateur pour
 * le mois $unMois à la nouvelle valeur $unEtat et passe la date de modif à 
 * la date d'aujourd'hui
 * @param resource $idCnx identifiant de connexion
 * @param string $unIdUtilisateur 
 * @param string $unMois mois sous la forme aaaamm
 * @param string $unEtat
 * @return void 
 */
function modifierEtatFicheFrais($idCnx, $unMois, $unIdUtilisateur, $unEtat) {
    $requete = "update FicheFrais set idEtat = '" . $unEtat .
            "', dateModif = now() where idUtilisateur ='" .
            $unIdUtilisateur . "' and mois = '" . $unMois . "'";
    mysqli_query($idCnx,$requete) or die(mysql_error());
}

/**
 * Retourne la requete d'obtention de la liste des visiteurs médicaux
 *
 * Retourne la requête d'obtention de la liste des visiteurs médicaux (id, nom et prenom)
 * @return string $requete
 */
function obtenirReqListeVisiteurs() {
    $requete = "select id, nom, prenom from utilisateur where idType='V' order by nom";
    return $requete;
}

/**
 * Modifie les quantités des éléments non forfaitisés d'une fiche de frais. 
 * Met à jour les éléments non forfaitisés contenus  
 * dans $desEltsHorsForfaits
 * @param resource $idCnx identifiant de connexion
 * @param array $desEltsHorsForfait tableau des éléments hors forfait
 * avec pour clés les identifiants des frais hors forfait
 * @return void  
 */
function modifierEltsHorsForfait($idCnx, $desEltsHorsForfait) {
    foreach ($desEltsHorsForfait as $cle => $val) {
        switch ($cle) {
            case 'id':
                $idFraisHorsForfait = $val;
                break;
            case 'libelle':
                $libelleFraisHorsForfait = $val;
                break;
            case 'date':
                $dateFraisHorsForfait = $val;
                break;
            case 'montant':
                $montantFraisHorsForfait = $val;
                break;
        }
    }
    $requete = "update LigneFraisHorsForfait"
            . " set libelle = '" . filtrerChainePourBD($libelleFraisHorsForfait) . "',"
            . " date = '" . convertirDateFrancaisVersAnglais($dateFraisHorsForfait) . "',"
            . " montant = " . $montantFraisHorsForfait
            . " where id = " . $idFraisHorsForfait;
    mysqli_query($idCnx,$requete);
}

/**
 * Modifie le nombre de justificatifs d'une fiche de frais
 *
 * Met à jour le nombre de justificatifs de la fiche de frais du visiteur $unIdUtilisateur pour
 * le mois $unMois à la nouvelle valeur $nbJustificatifs
 * @param resource $idCnx identifiant de connexion
 * @param string $unIdUtilisateur 
 * @param string $unMois mois sous la forme aaaamm
 * @param integer $nbJustificatifs
 * @return void 
 */
function modifierNbJustificatifsFicheFrais($idCnx, $unMois, $unIdUtilisateur, $nbJustificatifs) {
    $requete = "update FicheFrais set nbJustificatifs = " . $nbJustificatifs .
            " where idUtilisateur ='" . $unIdUtilisateur . "' and mois = '" . $unMois . "'";
    mysqli_query($idCnx,$requete);
}

/**
 * Reporte d'un mois une ligne de frais hors forfait
 * 
 * 
 * @param resource $idCnx identifiant de connexion
 * @param int $unIdLigneHF identifiant de ligne hors forfait
 * @return void
 */
function reporterLigneHorsForfait($idCnx, $unIdLigneHF) {
    mysqli_query($idCnx,'CALL reporterLigneFraisHF(' . $unIdLigneHF . ');');
}

/**
 * Cloture les fiches de frais antérieur au mois $unMois
 *
 * Cloture les fiches de frais antérieur au mois $unMois
 * et au besoin, créer une nouvelle de fiche de frais pour le mois courant
 * @param resource $idCnx identifiant de connexion
  * @param string $unMois mois sous la forme aaaamm
  * @return void 
 */
function cloturerFichesFrais($idCnx, $unMois) {
    $req = "SELECT idUtilisateur, mois FROM ficheFrais WHERE idEtat = 'CR' AND CAST(mois AS unsigned) < $unMois ;";
    $idJeuFichesFrais = mysqli_query($idCnx,$req);
    while ($lgFicheFrais = mysqli_fetch_array($idJeuFichesFrais)) {
        modifierEtatFicheFrais($idCnx, $lgFicheFrais['mois'], $lgFicheFrais['idUtilisateur'], 'CL');
        // Vérification de l'existence de la fiche de frais pour le mois courant
        $existeFicheFrais = existeFicheFrais($idCnx, $unMois, $lgFicheFrais['idUtilisateur']);
        // si elle n'existe pas, on la crée avec les éléments de frais forfaitisés à 0
        if (!$existeFicheFrais) {
            ajouterFicheFrais($idCnx, $unMois, $lgFicheFrais['idUtilisateur']);
        }
    }
}